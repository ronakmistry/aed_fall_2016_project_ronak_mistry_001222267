/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.Role.PurchasingRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class Purchasing extends Department{
    
    public Purchasing() {
        super(Type.Purchasing.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new PurchasingRole());
        return roles;
    }
}
