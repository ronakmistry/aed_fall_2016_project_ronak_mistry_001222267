/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.Department.PurchasingPackage.MaterialDirectory;
import Business.Role.PurchasingRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class SupplierDept extends Department{
    
    
    public SupplierDept() 
    {
        super(Type.Supplier.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new PurchasingRole());
        return roles;
    }
    
}
