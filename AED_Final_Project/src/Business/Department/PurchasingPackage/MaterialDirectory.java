/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.PurchasingPackage;

import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class MaterialDirectory {
    
    private ArrayList<Material> materialList;
    
    public MaterialDirectory()
    {
        materialList = new ArrayList<Material>();
    }

    public ArrayList<Material> getMaterialList() {
        return materialList;
    }

    private Material addMaterial(String name, int qty)
    {
        Material m = new Material();
        m.setMaterialName(name);
        m.setMaterialQty(qty);
        materialList.add(m);
        return m;
    }
    
    
    
}
