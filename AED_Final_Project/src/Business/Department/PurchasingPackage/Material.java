/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.PurchasingPackage;

/**
 *
 * @author Ronak
 */
public class Material {
    
    private int materialId;
    private static int count=0;
    private String materialName;
    private int materialQty;
    
    public Material()
    {
        count++;
        materialId=count;
    }
    public int getMaterialId() {
        return materialId;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Material.count = count;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public int getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(int materialQty) {
        this.materialQty = materialQty;
    }
    
    
}
