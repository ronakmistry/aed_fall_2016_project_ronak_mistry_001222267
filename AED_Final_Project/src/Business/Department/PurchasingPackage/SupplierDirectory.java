/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.PurchasingPackage;

import Business.Person.Supplier;
import java.util.ArrayList;
/**
 *
 * @author Ronak
 */
public class SupplierDirectory {
    
    private ArrayList<Supplier> supplierList;
    
    public SupplierDirectory()
    {
        supplierList = new ArrayList<Supplier>();
    }
    
    private Supplier addSupplier()
    {
        Supplier supp = new Supplier();
        supplierList.add(supp);
        return supp;
    }
}
