/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.Role.ProductionWorkerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class Production extends Department{
    
    public Production() {
        super(Type.Production.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new ProductionWorkerRole());
        return roles;
    }
    
}
