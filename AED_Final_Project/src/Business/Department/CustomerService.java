/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;


import Business.Role.CustomerServiceRole;
import Business.Role.Role;
import java.util.ArrayList;
/**
 *
 * @author Ronak
 */
public class CustomerService extends Department {
    
    
    public CustomerService() 
    {
        super(Type.CustomerService.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new CustomerServiceRole());
        return roles;
    }
    
}
