/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.ProductionPackage;

import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class InventoryCatalog {
    
    private ArrayList<Inventory> inventoryList;
    
    public InventoryCatalog()
    {
        inventoryList = new ArrayList<Inventory>();
    }

    public ArrayList<Inventory> getInventoryList() {
        return inventoryList;
    }
    
    public Inventory createAddInventory()
    {
        Inventory inv = new Inventory();
        inventoryList.add(inv);
        return inv;
    }
    
}
