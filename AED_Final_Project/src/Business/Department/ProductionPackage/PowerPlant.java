/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.ProductionPackage;

import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class PowerPlant {
    
    private int plantId;
    private static int count=0;
    private String plantName;
    private String plantCapacity;
    private InventoryCatalog inventoryList;
    
    public PowerPlant()
    {
        count++;
        plantId=count;
        inventoryList = new InventoryCatalog();
    }

    public int getPlantId() {
        return plantId;
    }

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getPlantCapacity() {
        return plantCapacity;
    }

    public void setPlantCapacity(String plantCapacity) {
        this.plantCapacity = plantCapacity;
    }
    
    
}
