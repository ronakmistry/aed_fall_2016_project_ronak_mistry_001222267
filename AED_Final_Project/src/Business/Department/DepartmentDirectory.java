/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.Department.Department.Type;
import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class DepartmentDirectory {

    private ArrayList<Department> departmentList;

    public DepartmentDirectory() {
        departmentList = new ArrayList<Department>();
    }

    public ArrayList<Department> getDepartmentList() {
        return departmentList;
    }

    public Department createDepartment(Type type) {
        Department department = null;
        if (type.getValue().equals(Type.Administration.getValue())) {
            department = new Administration();
            departmentList.add(department);
            
        } else if (type.getValue().equals(Type.Production.getValue())) {
            department = new Production();
            departmentList.add(department);
            
        } else if (type.getValue().equals(Type.Purchasing.getValue())) {
            department = new Purchasing();
            departmentList.add(department);
            
        } else if (type.getValue().equals(Type.CustomerService.getValue())) {
            department = new CustomerService();
            departmentList.add(department);
            
        } else if (type.getValue().equals(Type.Business.getValue())) {
            department = new BusinessDept();
            departmentList.add(department);
        } else if (type.getValue().equals(Type.Supplier.getValue())) {
            department = new SupplierDept();
            departmentList.add(department);
        }
        else if (type.getValue().equals(Type.Technician.getValue())) {
            department = new Technician();
            departmentList.add(department);
        }
        return department;
    }

}
