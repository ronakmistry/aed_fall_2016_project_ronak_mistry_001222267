/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.Department.BusinessDeptPackage.MRPHistory;
import Business.Role.BusinessRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class BusinessDept extends Department {
    
    private MRPHistory mrpHistory;
    
    public BusinessDept() {
        super(Type.Business.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new BusinessRole());
        return roles;
    }

    public MRPHistory getMrpHistory() {
        return mrpHistory;
    }
    

}
