/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department;

import Business.Person.PersonDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;
/**
 *
 * @author Ronak
 */
public abstract class Department {
    
    private String name;
    private WorkQueue workQueue;
    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int departmentID;
    private static int counter;
    
    public enum Type
    {
        Administration("Administration Department"),
        CustomerService("Customer Service Department"),
        Production("Production Department"),
        Purchasing("Purchasing Department"),
        Supplier("Supplier Department"),
        Business("Business Department"),
        Technician("Technician Department"),
        Customer("Customer Department");
        
        private String value;
        private Type(String value) 
        {
            this.value = value;
        }
        public String getValue() 
        {
            return value;
        }
    }
    
    public Department(String name) 
    {
        counter++;
        this.name = name;
        workQueue = new WorkQueue();
        personDirectory = new PersonDirectory();
        userAccountDirectory = new UserAccountDirectory();
        departmentID = counter;
    }
    
    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getDepartmentID() {
        return departmentID;
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
}
