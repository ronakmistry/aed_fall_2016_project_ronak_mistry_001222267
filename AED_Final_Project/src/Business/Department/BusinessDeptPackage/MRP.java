/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.BusinessDeptPackage;

import Business.Department.Department;
import Business.Person.Customer;
import Business.Person.Person;
import Business.Person.UsageStats.UsageStats;

/**
 *
 * @author Surabhi Patil
 */
public class MRP {
    
    private int personalUsage;
    private int commercialUsage;
    private int industrialUsage;
    private int solarUsage;
    private int normalUsage;
    private String month;
    private String year;
    private static int count=0;
    private int mrpID;
    
    public MRP()
    {
        count++;
        mrpID=count;
    }

    public int getMrpID() {
        return mrpID;
    }

    public int getPersonalUsage() {
        return personalUsage;
    }

    public void setPersonalUsage(int personalUsage) {
        this.personalUsage = personalUsage;
    }

    public int getCommercialUsage() {
        return commercialUsage;
    }

    public void setCommercialUsage(int commercialUsage) {
        this.commercialUsage = commercialUsage;
    }

    public int getIndustrialUsage() {
        return industrialUsage;
    }

    public void setIndustrialUsage(int industrialUsage) {
        this.industrialUsage = industrialUsage;
    }

    public int getSolarUsage() {
        return solarUsage;
    }

    public void setSolarUsage(int solarUsage) {
        this.solarUsage = solarUsage;
    }

    public int getNormalUsage() {
        return normalUsage;
    }

    public void setNormalUsage(int normalUsage) {
        this.normalUsage = normalUsage;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    
    
}
