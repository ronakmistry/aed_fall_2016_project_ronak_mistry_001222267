/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Department.BusinessDeptPackage;

import Business.Department.BusinessDept;
import Business.Department.CustomerService;
import Business.Department.Department;
import Business.EnergyProvider.EnergyProvider;
import Business.Person.Customer;
import Business.Person.Person;
import Business.Person.UsageStats.UsageStats;
import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class MRPHistory {
    
    private ArrayList<MRP> mrpList;
    
    public MRPHistory()
    {
        mrpList = new ArrayList<MRP>();
        
        
                
    }

    public ArrayList<MRP> getMrpList() {
        return mrpList;
    }
    
    public int personalUsage;
    public int commercialUsage;
    public int industrialUsage;
    public int solarUsage;
    public int normalUsage;
    public String month;
    public String year;
    public EnergyProvider energyProvider;
    
    public void runMRP(EnergyProvider eProv, String month, String year) {
        this.energyProvider = eProv;
        this.month = month;
        this.year = year;
        for (Department dept : eProv.getDepartmentDirectory().getDepartmentList()) {
            if (dept instanceof CustomerService) {
                for (Person person : dept.getPersonDirectory().getPersonList()) {
                    if (person instanceof Customer) {
                        
                        if (((Customer) person).getCustomerType() == "Personal" && ((Customer) person).getConsumptionType() == "Solar") {
                            for (UsageStats uStats : ((Customer) person).getuStats().getUsageStatsDirectory()) {
                                personalUsage = personalUsage + uStats.getUnitsConsumed();
                                solarUsage = solarUsage + uStats.getUnitsConsumed();
                            }
                        }
                        if (((Customer) person).getCustomerType() == "Personal" && ((Customer) person).getConsumptionType() == "Normal") {
                            for (UsageStats uStats : ((Customer) person).getuStats().getUsageStatsDirectory()) {
                                personalUsage = personalUsage + uStats.getUnitsConsumed();
                                normalUsage = normalUsage + uStats.getUnitsConsumed();
                            }
                        }
                        if (((Customer) person).getCustomerType() == "Commercial" && ((Customer) person).getConsumptionType() == "Solar") {
                            for (UsageStats uStats : ((Customer) person).getuStats().getUsageStatsDirectory()) {
                                commercialUsage = commercialUsage + uStats.getUnitsConsumed();
                                solarUsage = solarUsage + uStats.getUnitsConsumed();
                            }
                        }
                        if (((Customer) person).getCustomerType() == "Commercial" && ((Customer) person).getConsumptionType() == "Normal") {
                            for (UsageStats uStats : ((Customer) person).getuStats().getUsageStatsDirectory()) {
                                commercialUsage = commercialUsage + uStats.getUnitsConsumed();
                                normalUsage = normalUsage + uStats.getUnitsConsumed();
                            }
                        }
                        if (((Customer) person).getCustomerType() == "Industrial" && ((Customer) person).getConsumptionType() == "Solar") {
                            for (UsageStats uStats : ((Customer) person).getuStats().getUsageStatsDirectory()) {
                                industrialUsage = industrialUsage + uStats.getUnitsConsumed();
                                solarUsage = solarUsage + uStats.getUnitsConsumed();
                            }
                        }
                        if (((Customer) person).getCustomerType() == "Industrial" && ((Customer) person).getConsumptionType() == "Normal") {
                            for (UsageStats uStats : ((Customer) person).getuStats().getUsageStatsDirectory()) {
                                industrialUsage = industrialUsage + uStats.getUnitsConsumed();
                                normalUsage = normalUsage + uStats.getUnitsConsumed();
                            }
                        }
                    }
                }
            }
        }
        for (Department dept : eProv.getDepartmentDirectory().getDepartmentList()) 
        {
            if (dept instanceof BusinessDept)
            {
                ((BusinessDept) dept).getMrpHistory().createMRP(personalUsage, commercialUsage, industrialUsage, solarUsage, normalUsage, month, year);
            }
        }
    }

    public MRP createMRP(int personalUse, int commercialUse, int industrialUse, int solarUse, int normalUse, String month, String year)
    {
        MRP mrp = new MRP();
        mrp.setCommercialUsage(commercialUsage);
        mrp.setIndustrialUsage(industrialUsage);
        mrp.setMonth(month);
        mrp.setNormalUsage(normalUsage);
        mrp.setPersonalUsage(personalUsage);
        mrp.setSolarUsage(solarUsage);
        mrp.setYear(year);
        return mrp;
    }
    
}
