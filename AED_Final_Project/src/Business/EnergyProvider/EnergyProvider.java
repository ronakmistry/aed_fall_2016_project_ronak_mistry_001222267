/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.EnergyProvider;

import Business.Department.Department;
import Business.Department.DepartmentDirectory;

/**
 *
 * @author Surabhi Patil
 */
public abstract class EnergyProvider extends Department{
    
    private String name;
    private DepartmentDirectory departmentDirectory;
    private EnergyProviderType energyProviderType;

    public EnergyProvider(String name,EnergyProviderType type) {
        super(name);
        this.name = name;
        this.energyProviderType=type;
        this.departmentDirectory = new DepartmentDirectory();
    }
    
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DepartmentDirectory getDepartmentDirectory() {
        return departmentDirectory;
    }

    public enum EnergyProviderType{
        ElectricityProvider("Electricity Provider");
        
        private String value;
        
        private EnergyProviderType(String value){
            this.value=value;
        }
        public String getValue() {
            return value;
        }
        @Override
        public String toString(){
        return value;
    }
    }

    public EnergyProviderType getEnergyProviderType() {
        return energyProviderType;
    }

    public void setEnergyProviderType(EnergyProviderType energyProviderType) {
        this.energyProviderType = energyProviderType;
    }
    
    
}
