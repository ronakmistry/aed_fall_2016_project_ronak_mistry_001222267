/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.EnergyProvider;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class EnergyProviderDirectory {
    
    private ArrayList<EnergyProvider> energyProviderList;
    
    public EnergyProviderDirectory()
    {
        energyProviderList = new ArrayList<EnergyProvider>();
    }

    public ArrayList<EnergyProvider> getEnergyProviderList() {
        return energyProviderList;
    }

    public void setEnergyProviderList(ArrayList<EnergyProvider> energyProviderList) {
        this.energyProviderList = energyProviderList;
    }
    //Create EnergyProvider
    public EnergyProvider createAndAddEnergyProvider(String name,EnergyProvider.EnergyProviderType type)
    {
        EnergyProvider enterprise=null;
        if(type==EnergyProvider.EnergyProviderType.ElectricityProvider){
            enterprise=new ElectricityProvider(name);
            energyProviderList.add(enterprise);
        }
        return enterprise;
    }
    
}
