/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

import Business.Person.UsageStats.UsageStatsDirectory;

/**
 *
 * @author Ronak
 */
public class Customer extends Person {
    
    private String customerType;
    private String consumptionType;
    private int accountNumber;
    private static int count = 100000;
    private UsageStatsDirectory uStats;
    
    public Customer()
    {
        count++;
        accountNumber=count;
        uStats =  new UsageStatsDirectory();
    }

    public UsageStatsDirectory getuStats() {
        return uStats;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getConsumptionType() {
        return consumptionType;
    }

    public void setConsumptionType(String cunsomptionType) {
        this.consumptionType = cunsomptionType;
    }

    public int getAccountNumber() {
        return accountNumber;
    }
}
