/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

import Business.Department.PurchasingPackage.MaterialDirectory;

/**
 *
 * @author Ronak
 */
public class Supplier extends Person{
    
    private int supplierId;
    private static int count=10000;
    private MaterialDirectory materialDirectory;
    
    public Supplier()
    {
        count++;
        supplierId=count;
        materialDirectory = new MaterialDirectory();
    }

    public int getSupplierId() {
        return supplierId;
    }


    public MaterialDirectory getMaterialDirectory() {
        return materialDirectory;
    }

    public void setMaterialDirectory(MaterialDirectory materialDirectory) {
        this.materialDirectory = materialDirectory;
    }
    
    
}
