/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person.UsageStats;

import Business.Department.Department;
import Business.Person.Customer;
import Business.Person.Person;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ronak
 */
public class UsageStatsDirectory {
    
    private ArrayList<UsageStats> usageStatsDirectory;
    private ArrayList<String> monthArray;
    private ArrayList<String> yearArray;
    
    public UsageStatsDirectory()
    {
        usageStatsDirectory = new ArrayList<UsageStats>();
    }
    
    public static int generateInt(int range1, int range2)
    {
        Random rand = new Random();
        int randomNum = rand.nextInt((range2 - range1) + 1) + range1;

        return randomNum;
    }
    
    public void addUsageStats(Customer cust)
    {
        //Array to store months for sample data
        monthArray = new ArrayList<String>();
        monthArray.add("January");
        monthArray.add("February");
        monthArray.add("March");
        monthArray.add("April");
        monthArray.add("May");
        monthArray.add("June");
        monthArray.add("July");
        monthArray.add("August");
        monthArray.add("September");
        monthArray.add("October");
        monthArray.add("November");
        monthArray.add("December");
        
        //Array to store years for sample data
        yearArray = new ArrayList<String>();
        yearArray.add("2012");
        yearArray.add("2013");
        yearArray.add("2014");
        yearArray.add("2015");
        for (String year : yearArray)
        {
            for (String month : monthArray)
            {
                if(cust.getCustomerType()=="Personal" && cust.getConsumptionType()=="Solar")
                {
                    int usageStat = generateInt(100, 200);
                    cust.getuStats().createAddUsageStat(usageStat, month, year);
                }
                if(cust.getCustomerType()=="Personal" && cust.getConsumptionType()=="Normal")
                {
                    int usageStat = generateInt(100, 200);
                    cust.getuStats().createAddUsageStat(usageStat, month, year);
                }
                if(cust.getCustomerType()=="Commercial" && cust.getConsumptionType()=="Solar")
                {
                    int usageStat = generateInt(100, 200);
                    cust.getuStats().createAddUsageStat(usageStat, month, year);
                }
                if(cust.getCustomerType()=="Commercial" && cust.getConsumptionType()=="Normal")
                {
                    int usageStat = generateInt(100, 200);
                    cust.getuStats().createAddUsageStat(usageStat, month, year);
                }
                if(cust.getCustomerType()=="Industrial" && cust.getConsumptionType()=="Solar")
                {
                    int usageStat = generateInt(100, 200);
                    cust.getuStats().createAddUsageStat(usageStat, month, year);
                }
                if(cust.getCustomerType()=="Industrial" && cust.getConsumptionType()=="Normal")
                {
                    int usageStat = generateInt(100, 200);
                    cust.getuStats().createAddUsageStat(usageStat, month, year);
                }
            }
        }
    }
    
    //Call following function to create single usage statistic
    public UsageStats createAddUsageStat(int units, String month, String year)
    {
        UsageStats stats = new UsageStats();
        stats.setUnitsConsumed(units);
        stats.setBillMonth(month);
        stats.setBillYear(year);
        usageStatsDirectory.add(stats);
        return stats;
    }
    
    //Call following function to create a bill cycle for each customer
    public void createNewStat(Department department,String month, String year)
    {
        for(Person person : department.getPersonDirectory().getPersonList())
        {
            if (person instanceof Customer)
            {
                int stat = generateInt(100,200);
                createAddUsageStat(stat, month, year);
            }
        }
        
    }
    
    public ArrayList<UsageStats> getUsageStatsDirectory() {
        return usageStatsDirectory;
    }

    public void setUsageStatsDirectory(ArrayList<UsageStats> usageStatsDirectory) {
        this.usageStatsDirectory = usageStatsDirectory;
    }
    
}
