/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class PersonDirectory {

    private ArrayList<Person> personList;

    public PersonDirectory() {
        personList = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public Customer createCustomer(String name, String address, String email, String phoneNumber, String custType, String consumptionType)
    {
        Customer cust = new Customer();
        cust.setName(name);
        cust.setEmail(email);
        cust.setAddress(address);
        cust.setAccountType("Customer");
        cust.setPhone(phoneNumber);
        cust.setCustomerType(custType);
        cust.setConsumptionType(consumptionType);
        personList.add(cust);
        return cust;
    }
    
    public Supplier createSupplier(String name, String address, String email, String phoneNumber)
    {
        Supplier supplier = new Supplier();
        supplier.setName(name);
        supplier.setEmail(email);
        supplier.setAddress(address);
        supplier.setAccountType("Supplier");
        supplier.setPhone(phoneNumber);
        personList.add(supplier);
        return supplier;
    }
    
    public Employee createEmployee(String name, String address, String email, String phoneNumber)
    {
        Employee employee = new Employee();
        employee.setName(name);
        employee.setEmail(email);
        employee.setAddress(address);
        employee.setAccountType("Employee");
        employee.setPhone(phoneNumber);
        personList.add(employee);
        return employee;
    }
}
