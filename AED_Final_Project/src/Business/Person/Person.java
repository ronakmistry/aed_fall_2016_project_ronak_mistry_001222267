/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

/**
 *
 * @author Surabhi Patil
 */
public abstract class Person {
    
    private String name;
    private String accountType;
    private String address;
    private String email;
    private String phone;
    

    public enum Type
    {
        Employee("Employee"),
        Customer("Customer"),
        Supplier("Supplier");
        
        private String value;
        private Type(String value) 
        {
            this.value = value;
        }
        public String getValue() 
        {
            return value;
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    
    
}
