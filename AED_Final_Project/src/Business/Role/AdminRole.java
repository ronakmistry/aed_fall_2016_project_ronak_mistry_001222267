/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.PWD;
import Business.EnergyProvider.EnergyProvider;
import Business.Department.Department;
import Business.UserAccount.UserAccount;
import userInterface.Admin.AdminWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Ronak
 */
public class AdminRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Department department, EnergyProvider eProv, PWD business) {
        return new AdminWorkAreaJPanel(userProcessContainer, eProv);
    }
    
}
