/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Department.Department;
import Business.EnergyProvider.EnergyProvider;
import Business.PWD;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userInterface.Production.ProductionWorkAreaJPanel;

/**
 *
 * @author Surabhi Patil
 */
public class ProductionWorkerRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Department department, EnergyProvider eProv, PWD business) {
        return new ProductionWorkAreaJPanel(userProcessContainer, account, department, eProv);
    }
    
}
