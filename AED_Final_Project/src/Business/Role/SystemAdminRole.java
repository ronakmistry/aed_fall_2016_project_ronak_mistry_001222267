/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.PWD;
import Business.EnergyProvider.EnergyProvider;
import Business.Department.Department;
import Business.UserAccount.UserAccount;
import userInterface.SystemAdmin.SystemAdminWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Ronak
 */
public class SystemAdminRole extends Role{
    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Department Department, EnergyProvider eProv, PWD pwd) {
        return new SystemAdminWorkAreaJPanel(userProcessContainer, pwd);
    }
    
}
