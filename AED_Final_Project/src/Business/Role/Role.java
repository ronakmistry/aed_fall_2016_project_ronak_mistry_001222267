/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.PWD;
import Business.Department.Department;
import Business.Department.Department;
import Business.EnergyProvider.EnergyProvider;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Ronak
 */
public abstract class Role {
    
    public enum RoleType{
        Admin("Admin"),
        Business("Business"),
        Customer("Customer"),
        CustomerService("Customer Service"),
        ProductionWorker("Production Worker"),
        Purchasing("Purchasing"),
        Supplier("Supplier"),
        Technician("Technician");
        
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Department department, 
            EnergyProvider eProv, 
            PWD business);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
    
}