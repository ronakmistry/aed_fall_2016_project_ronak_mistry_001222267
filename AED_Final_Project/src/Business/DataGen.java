/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Department.Department;
import Business.Person.Customer;
import Business.Person.Person;
import Business.Person.UsageStats.UsageStats;

/**
 *
 * @author Ronak
 */
public class DataGen {
    
    Customer cust;
    
    public DataGen(Department dept, Customer customer)
    {
        this.cust=customer;
    }
    
    public void generateData(Customer customer)
    {
        customer.getuStats().addUsageStats(customer);
    }
    
    public void populateData(Department dept, Customer customer)
    {
        for (Person person : dept.getPersonDirectory().getPersonList())
        {
            if (person instanceof Customer)
            {
                generateData((Customer) person);
            }
        }
    }
    
}
