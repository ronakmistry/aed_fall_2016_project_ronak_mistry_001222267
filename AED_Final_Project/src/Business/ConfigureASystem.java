/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Person.Employee;
import Business.Person.Person;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author Ronak
 */
public class ConfigureASystem {
    
    public static PWD configure()
    {
        PWD power = PWD.getInstance();
        Person person = power.getPersonDirectory().createEmployee("Ronak","686 Parker Street","ronakmist@gmail.com","8577072957");
        UserAccount ua  = power.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", person, new SystemAdminRole());
        return power;
    }
}
