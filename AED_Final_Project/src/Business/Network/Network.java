/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.EnergyProvider.EnergyProviderDirectory;

/**
 *
 * @author Surabhi Patil
 */
public class Network {
    
    private String name;
    private EnergyProviderDirectory energyProviderDirectory;
    
    public Network()
    {
        energyProviderDirectory = new EnergyProviderDirectory();
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnergyProviderDirectory getEnergyProviderDirectory() {
        return energyProviderDirectory;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
}
