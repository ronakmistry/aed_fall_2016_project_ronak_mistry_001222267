/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Surabhi Patil
 */
public class ProductionChangeRequest extends WorkRequest{
 
    private String actionTaken;
    private int valueToSet;
    private String month;
    private String year;

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public int getValueToSet() {
        return valueToSet;
    }

    public void setValueToSet(int valueToSet) {
        this.valueToSet = valueToSet;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    @Override
    public String toString()
    {
        return "TEST";
    }
    
}
