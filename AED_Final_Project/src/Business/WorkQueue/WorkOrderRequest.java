/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Ronak
 */
public class WorkOrderRequest extends WorkRequest{
    
    private String taskRequested;
    private String location;
    private String taskType;

    public String getTaskRequested() {
        return taskRequested;
    }

    public void setTaskRequested(String taskRequested) {
        this.taskRequested = taskRequested;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }
    
    
    
}
