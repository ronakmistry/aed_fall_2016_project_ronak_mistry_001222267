/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Ronak
 */
public class OrderMaterialRequest extends WorkRequest{
    
    private String actionTaken;
    private int invoicedAmount;
    private int quantityProvided;

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public int getInvoicedAmount() {
        return invoicedAmount;
    }

    public void setInvoicedAmount(int invoicedAmount) {
        this.invoicedAmount = invoicedAmount;
    }

    public int getQuantityProvided() {
        return quantityProvided;
    }

    public void setQuantityProvided(int quantityProvided) {
        this.quantityProvided = quantityProvided;
    }
    
    
    
}
