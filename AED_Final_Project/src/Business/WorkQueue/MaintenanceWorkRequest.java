/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Surabhi Patil
 */
public class MaintenanceWorkRequest extends WorkRequest{
    
    private String workComplete;
    private String requestType;

    public String getWorkComplete() {
        return workComplete;
    }

    public void setWorkComplete(String workComplete) {
        this.workComplete = workComplete;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
    
    
}
