/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Surabhi Patil
 */
public class ReportOutageRequest extends WorkRequest{
    
    private String actionTaken;
    private String outageLocation;

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getOutageLocation() {
        return outageLocation;
    }

    public void setOutageLocation(String outageLocation) {
        this.outageLocation = outageLocation;
    }
    
}
