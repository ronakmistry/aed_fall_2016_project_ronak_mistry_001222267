/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.EnergyProvider.EnergyProvider;

/**
 *
 * @author Surabhi Patil
 */
public class ChangeConnectionRequest extends WorkRequest{
 
    private String requestRespone;
    private EnergyProvider newProvider;
    private String changeReason;
    private String requestType;
    
    public String getRequestRespone() {
        return requestRespone;
    }

    public void setRequestRespone(String requestRespone) {
        this.requestRespone = requestRespone;
    }

    public EnergyProvider getNewProvider() {
        return newProvider;
    }

    public void setNewProvider(EnergyProvider newProvider) {
        this.newProvider = newProvider;
    }

    public String getChangeReason() {
        return changeReason;
    }

    public void setChangeReason(String changeReason) {
        this.changeReason = changeReason;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
    
    
}
