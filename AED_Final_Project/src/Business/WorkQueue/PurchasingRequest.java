 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Surabhi Patil
 */
public class PurchasingRequest extends WorkRequest {
    
    private String actionTaken;
    private String materialToRequest;
    private int quantityToOrder;
    private String month;
    private String year;

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getMaterialToRequest() {
        return materialToRequest;
    }

    public void setMaterialToRequest(String materialToRequest) {
        this.materialToRequest = materialToRequest;
    }

    public int getQuantityToOrder() {
        return quantityToOrder;
    }

    public void setQuantityToOrder(int quantityToOrder) {
        this.quantityToOrder = quantityToOrder;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    
}
